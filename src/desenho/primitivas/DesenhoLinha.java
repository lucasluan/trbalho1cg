/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desenho.primitivas;
import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import java.util.Random;

public class DesenhoLinha implements GLEventListener{

 @Override
 public void display(GLAutoDrawable drawable) {
  
  GL2 gl = drawable.getGL().getGL2();
  
  gl.glClear(GL.GL_COLOR_BUFFER_BIT);
  gl.glColor3f(0, 1f, 0);
 
  gl.glBegin(GL.GL_LINES);
  gl.glVertex2i(0, 0);
  gl.glVertex2i(200, 150);
  gl.glEnd();
  
  gl.glEnable(GL2.GL_LINE_STIPPLE);
  gl.glLineStipple(1, (short) 0xF0F0);
  gl.glBegin(GL.GL_LINES);
  gl.glVertex2i(0, 2);
  gl.glVertex2i(210, 160);
  gl.glEnd();
  gl.glDisable(GL2.GL_LINE_STIPPLE);
  
  gl.glLineWidth(5f);
  //gl.glEnable(GL2.GL_LINE_STIPPLE);
  gl.glLineStipple(4, (short) 0xAAAA);
  gl.glBegin(GL.GL_LINES);
  gl.glVertex2i(0, 4);
  gl.glVertex2i(220, 170);
  gl.glEnd();
  gl.glDisable(GL2.GL_LINE_STIPPLE);
  
  gl.glEnable(GL2.GL_LINE_STIPPLE);
  gl.glLineStipple(4, (short) 0xAAAA);
  gl.glBegin(GL.GL_LINES);
  gl.glVertex2i(0, 6);
  gl.glVertex2i(230, 180);
  gl.glEnd();
  gl.glDisable(GL2.GL_LINE_STIPPLE);
  
  //gl.glEnable(GL2.GL_LINE_STIPPLE);
  gl.glLineStipple(4, (short) 0xAAAA);
  gl.glBegin(GL.GL_LINES);
  gl.glVertex2i(0, 8);
  gl.glVertex2i(240, 190);
  gl.glEnd();
  gl.glDisable(GL2.GL_LINE_STIPPLE);
  gl.glFlush();
   
 }

 @Override
 public void dispose(GLAutoDrawable arg0) {
  // TODO Auto-generated method stub
  
 }

 @Override
 public void init(GLAutoDrawable drawable) {
  
  GL2 gl = drawable.getGL().getGL2();
  
  gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  gl.glMatrixMode(GL2.GL_MATRIX_MODE);
  gl.glLoadIdentity();
  
  GLU glu = new GLU();
  glu.gluOrtho2D(0, 200f, 0f, 150f);
  
 }

 @Override
 public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4) {
  // TODO Auto-generated method stub
  
 }
 
 public static void main(String[] args) {
  
 
    GLProfile profile = GLProfile.get(GLProfile.GL2);
    
    GLCapabilities caps = new GLCapabilities(profile);
    
    GLCanvas canvas = new GLCanvas(caps); 
    canvas.addGLEventListener(new DesenhoLinha()); 
   
   
  
  JFrame frame = new JFrame("Desenho Primitivas");
  
  frame.add(canvas);
  frame.setSize(400, 300);
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  frame.setVisible(true);
  
 }

}